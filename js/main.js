var board = new Board();
var thinking = false;
var startTime = null;
var points = 0;

/*
 * Hebt die Spielsteine des Spielers, mit denen ein Gewinn erziehlt wurde, hervor
 */
function highlight_player() {
    $('.move').remove();
    var pieces = getWinningMovesPlayer(board);
    var circles;
    for (var i = 0; i < 3; i++) {
        circles = $('.col[data-id=' + pieces[i].x + ']').find('.circle').get();
        $(circles[circles.length - 1 - (Board.ZEILEN - pieces[i].y - 1)]).animate({'background-color': 'black'}, 500);
    }
    
    circles = $('.col[data-id=' + pieces[3].x + ']').find('.circle').get();
    $(circles[circles.length - 1 - (Board.ZEILEN - pieces[3].y - 1)]).animate({'background-color': 'black'}, 500, function() {
        points += 500;
        var name = prompt('Gewonnen (erreichte Punkte: ' + points + '), bitte geben Sie Ihren Namen ein', '');
        if (name !== '')
            insert(Date.now() - startTime, points, name);

    });
}

/*
 * Hebt die Spielsteine des Computers, mit denen ein Gewinn erziehlt wurde, hervor
 */
function highlight_computer() {
    $('.move').remove();
    var pieces = getWinningMovesComputer(board);
    var circles;

    for (var i = 0; i < 3; i++) {
        circles = $('.col[data-id=' + pieces[i].x + ']').find('.circle').get();
        $(circles[circles.length - 1 - (Board.ZEILEN - pieces[i].y - 1)]).animate({'background-color': 'black'}, 500);
    }

    circles = $('.col[data-id=' + pieces[3].x + ']').find('.circle').get();
    $(circles[circles.length - 1 - (Board.ZEILEN - pieces[3].y - 1)]).animate({'background-color': 'black'}, 500, function() {
        points -= 500;
        var name = prompt('Verloren (erreichte Punkte: ' + points + '), bitte geben Sie Ihren Namen ein', '');
        if (name !== '')
            insert(Date.now() - startTime, points, name);
    });
}

// Funktion überprüft, ob in der übergenen Spalte ein gültiger Zug vom Spieler durchgeführt werden kann.
// Falls dies zutrifft wird der 0-basierte Index der Zeile zurückgegebn, andernfalls wird null zurückgegeben
function possibleMove(col) {
    if (board.playerBoard[col][0] === true || board.computerBoard[col][0] === true)
        return null;

    for (var i = 1; i < Board.ZEILEN; i++) {
        if (board.playerBoard[col][i] === true || board.computerBoard[col][i] === true)
            return i - 1;
    }

    return Board.ZEILEN - 1;
}

function newGame() {
    thinking = false;
    startTime = null;
    points = 0;
    $('.circle').toggle('scale', 500, function() {
        $('.circle').remove();
        board = new Board();
    });
}

function insert(time, points, name) {
    var t = {};
    t['name'] = name;
    t['punkte'] = points;
    t['dauer'] = time;
    $.ajax({
        type: 'POST',
        url: 'db_insert.php',
        data: t,
        success: function(res) {
            console.log(res);
            location.reload();
        }
    });
}

// Wird beim Laden der Website ausgeführt
$(function() {
    $('.col').click(function() {
        if (thinking === false) {
            thinking = true;

            if (startTime === null)
                startTime = Date.now();

            var col = $(this).data().id;

            var pos = playerMove(board, col);

            if (pos !== null) {
                var elem = $('<div class="circle circle_player"></div>');
                $(this).prepend(elem);
                var value = $('#board').offset().top + 50;

                elem.css({'top': value + 'px'}).animate({'top': '' + ((pos * 100) + value) + 'px'}, 1000, 'easeOutBounce');

                // prüfen, ob Spieler gewonnen hat
                if (playerWon(board)) {
                    elem.promise().done(function() {

                        // Spielsteine, mit denen der Gewinn erziehlt wurde, hervorheben
                        highlight_player();
                    });
                    return;
                }

                // auf Unentschieden prüfen
                if (!possibleMoves(board)) {
                    elem.promise().done(function() {
                        var name = prompt('Unentschieden (erreichte Punkte: ' + points + '), bitte geben Sie Ihren Namen ein', '');
                        if (name != '')
                            insert(Date.now() - startTime, points, name);
                    });
                    return;
                }

                // Computer Zug berechnen
                var time = parseInt($('#time_limit').val());
                if (isNaN(time)) {
                    $('#time_limit').val('0');
                    time = 0;
                }

                var worker = new Worker('js/alpha_beta_worker.js');
                worker.onmessage = function(e) {
                    worker.terminate();
                    var move = e.data.move;

                    if (e.data.log !== null)
                        console.log(e.data.log);

                    var _col;
                    $.each($('.col'), function() {
                        if ($(this).data().id === move.x)
                            _col = this;
                    });
                    elem = $('<div class="circle circle_computer"></div>');
                    $(_col).prepend(elem);
                    elem.css({'top': value + 'px'}).animate({'top': '' + ((move.y * 100) + value) + 'px'}, 1000, 'easeOutBounce');

                    board.computerBoard[move.x][move.y] = true;
                    console.log("Ausgewählter Zug: {Spalte: " + move.x + ", Zeile: " + move.y + "}");
                    console.log("Nodes: " + e.data.nodes);
                    console.log("NPS: " + (e.data.nodes / (time) * 1000));
                    console.log("Depth: " + e.data.depth);

                    points += e.data.depth;
                    if (computerWon(board)) {
                        elem.promise().done(function() {

                            // Spielsteine, mit denen der Gewinn erziehlt wurde, hervorheben
                            highlight_computer();


                        });
                        return;
                    }

                    // auf Unentschieden prüfen
                    if (!possibleMoves(board)) {
                        elem.promise().done(function() {
                            var name = prompt('Unentschieden (erreichte Punkte: ' + points + '), bitte geben Sie Ihren Namen ein', '');
                            if (name != '')
                                insert(Date.now() - startTime, points, name);
                        });
                        return;
                    }

                    thinking = false;
                };
                worker.postMessage({board: board, value: time});
            } else
                thinking = false;
        }
    });

    $('.col').hover(function() {
        // Maus hinen
        var pos = possibleMove($(this).data().id);

        if (pos !== null) {
            var elem = $('<div class="circle circle_player move"></div>');
            $(this).prepend(elem);
            elem.css({'top': ((pos * 100) + $('#board').offset().top + 50) + 'px', opacity: '0'});
            $(this).prepend(elem);
            elem.animate({opacity: '0.5'}, 500);
        }
    }, function() {
        // Maus hinaus
        //$('.move').animate({opacity:'0'}, 250, function() {this.remove()});
        $('.move').remove();
    });

    $('#new-game').click(function() {
        newGame();
    });
});