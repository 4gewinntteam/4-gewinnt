function get_2d_bool_array() {
    var tmp = [];
    for (var i = 0; i < Board.SPALTEN; i++) {
        tmp[i] = [];
        for (var j = 0; j < Board.ZEILEN; j++)
            tmp[i][j] = false;
    }
    return tmp;
}

/*
 Klasse für Spielbrett (aktuelle Stellung für Spieler und Computer)
 Spalten == x-Index
 Zeilen == y-Index
 */
function Board() {
    this.playerBoard = get_2d_bool_array();
    this.computerBoard = get_2d_bool_array();
}

Board.SPALTEN = 7;
Board.ZEILEN = 6;

function playerMove(board, col) {
    for (var i = Board.ZEILEN - 1; i >= 0; i--) {
        if (board.playerBoard[col][i] === false && board.computerBoard[col][i] === false) {
            board.playerBoard[col][i] = true;
            return i;
        }
    }
    return null;
    /*
     for (var i = 0; i < Board.ZEILEN; i++)
     if (this.playerBoard[col][i] === true || this.computerBoard[col][i] === true) {
     if (i !== 0) {
     this.playerBoard[col][i - 1] = true;
     return (i - 1);
     } else {
     return null;
     }
     }
     
     this.playerBoard[col][Board.ZEILEN - 1] = true;
     return (Board.ZEILEN - 1);
     */
}

function deepCopy(position) {
    var tmp = new Board();
    for (var i = 0; i < Board.SPALTEN; i++)
        for (var j = 0; j < Board.ZEILEN; j++) {
            tmp.playerBoard[i][j] = position.playerBoard[i][j];
            tmp.computerBoard[i][j] = position.computerBoard[i][j];
        }

    return tmp;
}

function playerWon(position) {
    var counter = 0;

    // ZEILEN
    for (var i = 0; i < Board.ZEILEN; counter = 0, i++)
        for (var j = 0; j < Board.SPALTEN; j++)
            if (position.playerBoard[j][i] === true) {
                if (++counter === 4)
                    return true;
            } else {
                counter = 0;
            }

    // SPALTEN
    counter = 0;
    for (var i = 0; i < Board.SPALTEN; counter = 0, i++)
        for (var j = 0; j < Board.ZEILEN; j++)
            if (position.playerBoard[i][j] === true) {
                if (++counter === 4)
                    return true;
            } else {
                counter = 0;
            }

    // Diagonal von links oben nach rechts unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = 0; j < Board.SPALTEN - 3; j++) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.playerBoard[j + k][i + k] === true) {
                    if (++counter === 4)
                        return true;
                } else
                    break;
        }

    // Diagonal von rechts oben nach links unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = Board.SPALTEN - 1; j >= 3; j--) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.playerBoard[j - k][i + k] === true) {
                    if (++counter === 4)
                        return true;
                } else
                    break;
        }

    return false;
}

function computerWon(position) {
    var counter = 0;

    // ZEILEN
    for (var i = 0; i < Board.ZEILEN; counter = 0, i++)
        for (var j = 0; j < Board.SPALTEN; j++)
            if (position.computerBoard[j][i] === true) {
                if (++counter === 4)
                    return true;
            } else {
                counter = 0;
            }

    // SPALTEN
    counter = 0;
    for (var i = 0; i < Board.SPALTEN; counter = 0, i++)
        for (var j = 0; j < Board.ZEILEN; j++)
            if (position.computerBoard[i][j] === true) {
                if (++counter === 4)
                    return true;
            } else {
                counter = 0;
            }

    // Diagonal von links oben nach rechts unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = 0; j < Board.SPALTEN - 3; j++) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.computerBoard[j + k][i + k] === true) {
                    if (++counter === 4)
                        return true;
                } else
                    break;
        }

    // Diagonal von rechts oben nach links unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = Board.SPALTEN - 1; j >= 3; j--) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.computerBoard[j - k][i + k] === true) {
                    if (++counter === 4)
                        return true;
                } else
                    break;
        }

    return false;
}

function possibleMoves(position) {
    for (var i = 0; i < Board.SPALTEN; i++)
        for (var j = 0; j < Board.ZEILEN; j++)
            if (position.playerBoard[i][j] === false && position.computerBoard[i][j] === false)
                return true;
    return false;
}

/*
 * gibt die Koordinaten der Spielsteine in einem Array zur�ck, mit der der Spieler gewonnen hat
 */
function getWinningMovesPlayer(position) {
    var pieces = [];
    var counter = 0;

    // Zeilen
    for (var i = 0; i < Board.ZEILEN; counter = 0, i++)
        for (var j = 0; j < Board.SPALTEN; j++)
            if (position.playerBoard[j][i] === true) {
                if (++counter === 4) {

                    for (var k = 0; k < 4; k++)
                        pieces.push({x: j - k, y: i});
                    return pieces;
                }
            } else {
                counter = 0;
            }

    // Spalten
    counter = 0;
    for (var i = 0; i < Board.SPALTEN; counter = 0, i++)
        for (var j = 0; j < Board.ZEILEN; j++)
            if (position.playerBoard[i][j] === true) {
                if (++counter === 4) {
                    for (var k = 0; k < 4; k++)
                        pieces.push({x: i, y: j - k});

                    return pieces;
                }
            } else {
                counter = 0;
            }

    // Diagonal von links oben nach rechts unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = 0; j < Board.SPALTEN - 3; j++) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.playerBoard[j + k][i + k] === true) {
                    if (++counter === 4) {
                        for (var l = 0; l < 4; l++)
                            pieces.push({x: j + l, y: i + l});

                        return pieces;
                    }
                } else
                    break;
        }

    // Diagonal von rechts oben nach links unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = Board.SPALTEN - 1; j >= 3; j--) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.playerBoard[j - k][i + k] === true) {
                    if (++counter === 4) {

                        for (var l = 0; l < 4; l++)
                            pieces.push({x: j - l, y: i + l});

                        return pieces;
                    }
                } else
                    break;
        }

    return false;
}

/*
 * gibt die Koordinaten der Spielsteine zur�ck, mit der der Computer gewonnen hat
 */
function getWinningMovesComputer(position) {
    var pieces = [];
    var counter = 0;

    // ZEILEN
    for (var i = 0; i < Board.ZEILEN; counter = 0, i++)
        for (var j = 0; j < Board.SPALTEN; j++)
            if (position.computerBoard[j][i] === true) {
                if (++counter === 4) {

                    for (var k = 0; k < 4; k++)
                        pieces.push({x: j - k, y: i});

                    return pieces;
                }
            } else {
                counter = 0;
            }

    // SPALTEN
    counter = 0;
    for (var i = 0; i < Board.SPALTEN; counter = 0, i++)
        for (var j = 0; j < Board.ZEILEN; j++)
            if (position.computerBoard[i][j] === true) {
                if (++counter === 4) {
                    for (var k = 0; k < 4; k++)
                        pieces.push({x: i, y: j - k});

                    return pieces;
                }
            } else {
                counter = 0;
            }

    // Diagonal von links oben nach rechts unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = 0; j < Board.SPALTEN - 3; j++) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.computerBoard[j + k][i + k] === true) {
                    if (++counter === 4) {

                        for (var l = 0; l < 4; l++)
                            pieces.push({x: j + l, y: i + l});

                        return pieces;
                    }
                } else
                    break;
        }

    // Diagonal von rechts oben nach links unten
    for (var i = 0; i < Board.ZEILEN - 3; i++)
        for (var j = Board.SPALTEN - 1; j >= 3; j--) {
            counter = 0;
            for (var k = 0; k < 4; k++)
                if (position.computerBoard[j - k][i + k] === true) {
                    if (++counter === 4) {

                        for (var l = 0; l < 4; l++)
                            pieces.push({x: j - l, y: i + l});

                        return pieces;
                    }
                } else
                    break;
        }

    return false;
}