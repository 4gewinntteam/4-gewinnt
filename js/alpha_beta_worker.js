var staged_best_move;
var bestMove;
var current_depth;
var nodes;
var start_time;
var time;
var log_data;
var smallest_alpha, smallest_beta;
var start_board;
importScripts('board.js');

function maxDepth(position) {
    var c = 0;
    for (var i = 0; i < Board.SPALTEN; i++)
        for (var j = 0; j < Board.ZEILEN; j++) {
            if (position.playerBoard[i][j] === false || position.computerBoard[i][j] === false)
                c++;
        }
    return c;
}

function get_random_move() {
    var moves = [];
    for (var x = 0; x < Board.SPALTEN; x++)
        for (var y = Board.ZEILEN - 1; y >= 0; y--) {
            if ((start_board.playerBoard[x][y] === false) && (start_board.computerBoard[x][y] === false)) {
                start_board.computerBoard[x][y] = true;
                if (computerWon(start_board)) {
                    return {x: x, y: y};
                }
                else
                    moves.push({x: x, y: y});
                start_board.computerBoard[x][y] = false;
                break;
            }
        }
    
    return moves[0];
}

onmessage = function(e) {
    // nur solange suchen, bis das Zeitlimit erreicht ist
    if (typeof e.data.value === 'number') {
        time = e.data.value;
        start_board = deepCopy(e.data.board);
        current_depth = 1;
        nodes = 0;
        moves = [];
        start_time = Date.now();
        log_data = [];
        smallest_alpha = -Infinity;
        smallest_beta = Infinity;
        var max_depth = maxDepth(start_board);
        while (true) {
            max(current_depth, smallest_alpha - 20, smallest_beta + 20, deepCopy(start_board));
            
            if (current_depth >= max_depth) {
                staged_best_move = {x: bestMove.x, y: bestMove.y};
                post_data();
            }
            
            staged_best_move = {x: bestMove.x, y: bestMove.y};
            
            current_depth++;
        }
    }
};

function post_data() {
    if (staged_best_move === undefined) {
        staged_best_move = get_random_move();
    }
    postMessage({move: staged_best_move, nodes: nodes, depth: current_depth - 1, log: null});
}

function max(depth, alpha, beta, position) {
    if ((Date.now() - start_time) >= time) {
        post_data();
    }
    
    if (alpha > smallest_alpha)
        smallest_alpha = alpha;
        if (beta < smallest_beta)
        smallest_beta = beta;
    
    nodes++;
   
    if (depth === 0)
        return evaluate(position);
    if (computerWon(position))
        return 1000;
    if (playerWon(position))
        return -1000;

    var moves = [];
    for (var x = 0; x < Board.SPALTEN; x++)
        for (var y = Board.ZEILEN - 1; y >= 0; y--) {
            if ((position.playerBoard[x][y] === false) && (position.computerBoard[x][y] === false)) {
                position.computerBoard[x][y] = true;
                if (computerWon(position)) {
                    moves.unshift({x: x, y: y});
                }
                else
                    moves.push({x: x, y: y});
                position.computerBoard[x][y] = false;
                break;
            }
        }
    
    if (moves.length === 0) {
        return evaluate(position);
    }
    
    var maxValue = alpha;
    for (var i = 0; i < moves.length; i++) {
        position.computerBoard[moves[i].x][moves[i].y] = true;
        
        var tmp = {playerBoard: [], computerBoard: []};
        for (var _i = 0; _i < Board.SPALTEN; _i++){
            tmp.playerBoard[_i] = [];
            tmp.computerBoard[_i] = [];
            for (var j = 0; j < Board.ZEILEN; j++) {
                tmp.playerBoard[_i][j] = position.playerBoard[_i][j];
                tmp.computerBoard[_i][j] = position.computerBoard[_i][j];
            }
        }
        var value = min(depth - 1, maxValue, beta, tmp);
        
        position.computerBoard[moves[i].x][moves[i].y] = false;
        
        if (value > maxValue) {
            maxValue = value;
            if (maxValue >= beta)
                break;
            if (depth == current_depth) {
                bestMove = {x: moves[i].x, y: moves[i].y};
            }
        }
    }
    return maxValue;
}

function min(depth, alpha, beta, position) {
    if ((Date.now() - start_time) >= time) {
        post_data();
    }
    
    if (beta < smallest_beta)
        smallest_beta = beta;
    if (alpha > smallest_alpha)
        smallest_alpha = alpha;
    
    nodes++;
    
    if (depth === 0)
        return evaluate(position);
    if (computerWon(position))
        return 1000;
    if (playerWon(position))
        return -1000;
    
    var moves = [];
    for (var x = 0; x < Board.SPALTEN; x++)
        for (var y = Board.ZEILEN - 1; y >= 0; y--) {
            if ((position.playerBoard[x][y] === false) && (position.computerBoard[x][y] === false)) {
                position.playerBoard[x][y] = true;
                if (playerWon(position)) {
                    moves.unshift({x: x, y: y});
                }
                else
                    moves.push({x: x, y: y});
                position.playerBoard[x][y] = false;
                break;
            }
        }
    
    if (moves.length === 0)
        return 0;
    
    var minValue = beta;
    for (var i = 0; i < moves.length; i++) {
        position.playerBoard[moves[i].x][moves[i].y] = true;
        
        var tmp = {playerBoard: [], computerBoard: []};
        for (var _i = 0; _i < Board.SPALTEN; _i++){
            tmp.playerBoard[_i] = [];
            tmp.computerBoard[_i] = [];
            for (var j = 0; j < Board.ZEILEN; j++) {
                tmp.playerBoard[_i][j] = position.playerBoard[_i][j];
                tmp.computerBoard[_i][j] = position.computerBoard[_i][j];
            }
        }
        
        var value = max(depth - 1, alpha, minValue, tmp);
        
        position.playerBoard[moves[i].x][moves[i].y] = false;
        
        if (value < minValue) {
            minValue = value;
            if (minValue <= alpha)
                break;
        }
    }
    return minValue;
}

// Gibt die Wertung der übergebenen Position aus der Sicht des Computers zurück
function evaluate(position) {
    
    var spieler_score = 0, computer_score = 0, score, spalte, zeile, counter, reihe;
    
    // Spieler
    
    //Zeilen
    for (spalte = 0; spalte < Board.SPALTEN; spalte++)
        for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4) {
            reihe = true;
            counter = 0;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    // SPALTEN
    for (zeile = 0; zeile < Board.ZEILEN; zeile++)
        for (spalte = 0; spalte + 3 < Board.SPALTEN; spalte += 4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte + i][zeile]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte + i][zeile]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    //Diagonal von links oben nach rechts unten
    for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4)
        for (spalte = 0; spalte + 3 < Board.SPALTEN; spalte += 4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte + i][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte + i][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    //Diagonal von rechts oben nach links unten
    for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4)
        for (spalte = Board.SPALTEN - 1; spalte >= 3; spalte -=4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte - i][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte - i][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    // Computer
    //Zeilen
    for (spalte = 0; spalte < Board.SPALTEN; spalte++)
        for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4) {
            reihe = true;
            counter = 0;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    // Board.SPALTEN
    for (zeile = 0; zeile < Board.ZEILEN; zeile++)
        for (spalte = 0; spalte + 3 < Board.SPALTEN; spalte += 4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte + i][zeile]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte + i][zeile]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    //Diagonal von links oben nach rechts unten
    for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4)
        for (spalte = 0; spalte + 3 < Board.SPALTEN; spalte += 4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte + i][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte + i][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    //Diagonal von rechts oben nach links unten
    for (zeile = 0; zeile + 3 < Board.ZEILEN; zeile += 4)
        for (spalte = Board.SPALTEN - 1; spalte >= 3; spalte -=4) {
            counter = 0;
            reihe = true;
            score = 0;
            for (var i = 0; i < 4; i++) {
                if (position.playerBoard[spalte - i][zeile + i]) {
                    score++;
                    counter++;
                } else if (position.computerBoard[spalte - i][zeile + i]) {
                    reihe = false;
                    break;
                } else {
                    counter++;
                }
            }
            
            if (reihe && (counter === 4))
                computer_score += score;
        }
    
    //hash[hash_key] = computer_score - spieler_score;
    return computer_score - spieler_score;
}

// Gibt ein Array mit den möglichen Zügen zurück, die in der übergebenen Position gespielt werden können
// Die Züge, bei denen der Computer gewinnt, befinden sich als erste im Array
function getMovesComputer(position) {
    var moves = [];

    for (var x = 0; x < Board.SPALTEN; x++)
        for (var y = Board.ZEILEN - 1; y >= 0; y--) {
            if ((position.playerBoard[x][y] === false) && (position.computerBoard[x][y] === false)) {
                position.computerBoard[x][y] = true;
                if (computerWon(position))
                    moves.unshift({x: x, y: y});
                else
                    moves.push({x: x, y: y});
                position.computerBoard[x][y] = false;
                break;
            }
        }
    return moves;
}

// Gibt ein Array mit den möglichen Zügen zurück, die in der übergebenen Position gespielt werden können
// Die Züge, bei denen der Spieler gewinnt, befinden sich als erste im Array
function getMovesPlayer(position) {
    var moves = [];

    for (var x = 0; x < Board.SPALTEN; x++)
        for (var y = Board.ZEILEN - 1; y >= 0; y--) {
            if ((position.playerBoard[x][y] === false) && (position.computerBoard[x][y] === false)) {
                position.playerBoard[x][y] = true;
                if (playerWon(position))
                    moves.unshift({x: x, y: y});
                else
                    moves.push({x: x, y: y});
                position.playerBoard[x][y] = false;
                break;
            }
        }
    
    return moves;
}