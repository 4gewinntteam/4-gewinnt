<?php
    include "db_access.php";
    $dhb = new PDO("mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass);
    date_default_timezone_set('UTC');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css"></link>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="js/board.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
    <div id="logo">
        <img src="img/Logo.png">
    </div>
    <div class="container">
        <div id="board">
            <?php
            // Hinzufügen von 7 Spalten
            echo '<div class="col col-left" data-id="0"></div>';
            for ($i = 1; $i < 6; $i++) {
                echo '<div class="col" data-id="' . $i .'">';
                /*
                for ($j = 0; $j < 6; $j++)
                    echo '<div class="circle_empty"></div>';
                */
                echo '</div>';
            }
            echo '<div class="col col-right"" data-id="6"></div>';
            ?>
        </div>
        <div id="settings">
            <button id="new-game">Neues Spiel</button>
            <input type="number" id="time_limit" value="1000" min="0">
        </div>
        <div id="table">
            <!--Tabelle zum Anzeigen der 10 besten Spieler -->
            <table>
                <tr><th></th><th>Name</th><th>Punkte</th><th>Spieldauer</th><th>Datum</th></tr>
                    <?php

                    //Auslesen der Spielerdaten aus der DB
                    $sql = $dhb->query('SELECT Name, Punkte, Dauer, Datum FROM Spieler LEFT JOIN Wertung ON Wertung.SpielerID = Spieler.ID ORDER BY Punkte DESC;');
                    $res = $sql->fetchAll(PDO::FETCH_NUM);
                    // Zähler für Reihung der Platzierung
                    $counter = 1;

                    foreach ($res as $row) {
                            // Anzeigen der Daten in der Tabelle 
                            echo '<tr><td>'. $counter++ .'</td><td>'. $row[0] . '</td><td>'.$row[1].'</td><td>' . round($row[2] / 1000, 2) . ' s</td><td>'. $row[3] . '</td></tr>';

                            if ($counter > 10)
                                break;
                        
                    }
                    // Leere Einträge, falls nicht 10 Spieler in der DB sind 
                    for ($i = $counter; $i <= 10; $i++) {
                        echo '<tr><td>'.$i.'</td><td></td><td></td><td></td><td></td></tr>';
                    }
                    ?>
            </table>
        </div>
        
    </div>
    <!--Footer-->
    <footer>
        <!-- Links für Footer -->
        <div id="links">
            <a href="kontakt.php">Kontakt</a>
        </div>
        <!-- Zusatzinformationen -->
        <p>Version 1.0</p>
        <p>Copyright ©2014 Genger, Hetzenberger & Walzer. Alle Rechte vorbehalten.</p>
    </footer>
</body>
</html>