-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 06. Mai 2014 um 14:26
-- Server Version: 5.6.11
-- PHP-Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `highscore`
--
CREATE DATABASE IF NOT EXISTS `highscore` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `highscore`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spieler`
--

CREATE TABLE IF NOT EXISTS `spieler` (
  `ID` int(64) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `spieler`
--

INSERT INTO `spieler` (`ID`, `Name`) VALUES
(1, 'Hetzinho'),
(2, 'Gengerinho');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wertung`
--

CREATE TABLE IF NOT EXISTS `wertung` (
  `ID` int(64) NOT NULL AUTO_INCREMENT,
  `SpielerID` int(64) NOT NULL,
  `Punkte` double DEFAULT NULL,
  `Dauer` bigint(20) DEFAULT NULL,
  `Datum` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SpielerID` (`SpielerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `wertung`
--

INSERT INTO `wertung` (`ID`, `SpielerID`, `Punkte`, `Dauer`, `Datum`) VALUES
(1, 1, 200, 500, '2014-05-06');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `wertung`
--
ALTER TABLE `wertung`
  ADD CONSTRAINT `wertung_ibfk_1` FOREIGN KEY (`SpielerID`) REFERENCES `spieler` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
