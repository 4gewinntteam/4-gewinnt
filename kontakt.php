<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style_kontakt.css"></link>
</head>
<body>
	<div id="logo">
        <img src="img/Logo.png">
    </div>

	<h2>Kontakt:</h2>
    
	<table>
		<tr><th>Hetzenberger Matthias</th></tr>
		<tr><td>3500 Krems an der Donau, Austria</td></tr>
		<tr><td>Bründlgraben 3</td></tr>
		<tr><td>E-Mail: <a href="mailto:mhetzenberger@gmail.com">mhetzenberger@gmail.com</a></td></tr>
	</table>

	<table>
		<tr><th>Walzer Philipp</th></tr>
		<tr><td>3511 Furth/Göttweig, Austria</td></tr>
		<tr><td>Untere Landstraße 17</td></tr>
		<tr><td>E-Mail: <a href="mailto:walzerphilipp@gmail.com">walzerphilipp@gmail.com</a></td></tr>
	</table>

	<table>
		<tr><th>Genger Christopher</th></tr>
		<tr><td>3473 Mühlbach am Manhartsberg, Austria</td></tr>
		<tr><td>Hauptstraße 112</td></tr>
		<tr><td>E-Mail: <a href="mailto:christophergenger@gmail.com">christophergenger@gmail.com</a></td></tr>
	</table>

	<footer>
		<a href="index.php">Zurück zur Hauptseite</a>
	</footer>
</body>
</html>