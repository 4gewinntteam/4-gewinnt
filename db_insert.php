<?php 
	// Einfügen der Datenbank-Zugriffsdaten
	include "db_access.php";

	// Erstellen eines Datenbankobjektes
	$dbh = new PDO("mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass);

	// Spielernamen in DB einfügen
    $sql = "INSERT INTO Spieler(Name) VALUES ('$_POST[name]')";
    $dbh->exec($sql);

    // Auslesen der ID des letzter Spieler 
	$sql = $dbh->query("SELECT MAX(id) from spieler WHERE name = '$_POST[name]'");
	$res = $sql->fetchAll(PDO::FETCH_NUM);

	// SpielerID
    $id = $res[0][0];

    // Einfügen der gespielten Daten in die DB
	$sql = "INSERT INTO Wertung(SpielerID, Punkte, Dauer, Datum) VALUES ($id, $_POST[punkte], $_POST[dauer], CURDATE())";
	$dbh->exec($sql);
?>